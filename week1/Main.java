package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import  java.util.Scanner;
public class Main {
    private static void Test(String path)  throws FileNotFoundException{
        Scanner in  = new Scanner(new File(path));
        Shape sh = new Shape() ;
        while(in.hasNextDouble()) {
            double x =in.nextDouble();
            double y = in.nextDouble();
            Point a = new Point(x,y);
            sh.addPoint(a);
           
        }
        System.out.println("Perimeter: " + sh.calculatePerimeter(sh)); 
      
        System.out.println("Average Length: " + sh.getAverageLength(sh));
        System.out.println("Largest Side:" + sh.getLargestSide(sh));
        System.out.println("Number of Points: " + sh.getNumPoints());
    }
    public static void main(String[] args ) throws FileNotFoundException{
        System.out.println("1st test");
        Test("src/com/company/test1.txt");
        System.out.println("End of the 1st test");

        System.out.println("\n");

        System.out.println("2nd test");
        Test("src/com/company/test2.txt");
        System.out.println("End of the 2nd test");

    }
}
