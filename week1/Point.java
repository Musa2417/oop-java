package com.company;

import org.jetbrains.annotations.NotNull;

public class Point {

    public double x;
    public double y;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double GetX(){
        return x;
    }

    public double GetY(){
        return y;
    }

    public double distance(@NotNull Point B){
        return Math.sqrt( ( x- B.GetX() ) * ( x - B.GetX() ) + ( y - B.GetY() ) * ( y - B.GetY()) );
    }

}
