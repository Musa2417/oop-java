package com.company;

import org.jetbrains.annotations.NotNull;

import java.util.Vector;

public class Shape {
    public static Vector<Point> s;
    public int counter;
    public Shape(){
        s = new Vector<Point>();
        counter = 0;
    }

    public void addPoint(@NotNull Point A){
        s.addElement(A);
        counter = counter + 1;
    }

    public Vector<Point> getPoints(){
        return  s;
    }

    public Point getLastPoint(){
        return s.lastElement();
    }

    public double calculatePerimeter (@NotNull Shape s) {
        double Perim = 0.0;
        Point prevPoint = s.getLastPoint();
        for (Point currPoint : s.getPoints()) {
            double Dist = prevPoint.distance(currPoint);
            Perim = Perim + Dist;
            prevPoint = currPoint;
        }

        return Perim;
    }

    public int getNumPoints () {
        return counter;
    }

    public double getAverageLength(@NotNull Shape s) {
        double P = s.calculatePerimeter(s);
        double average = P / counter;
        return average;
    }

    public double getLargestSide(@NotNull Shape s) {
        double Perim = 0.0;
        double maxSide = -15555555555555.0;
        Point prevPt = s.getLastPoint();
        for (Point currPt : s.getPoints()) {
            double currDist = prevPt.distance(currPt);
            if(currDist > maxSide){
                maxSide = currDist;
            }
            prevPt = currPt;
        }
        return maxSide;
    }
}
